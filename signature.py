
def signature (B,x0,y0,f=1):
    """
    Created on Fri Jun 10 12:19:56 2022

    @author: Delle Donne Julian


    Funcion que calcula la firma de un objeto
    Se debe ingresar:
                    - Los puntos (i,j) del contorno de la imagen en B.
                    - Su centro en (x0,y0).
                    - Flag f = 1 grafica la firma. Por defecto es 1.
    La funcion devuelve la magnitud (ST) y face (ANGLE) de cada punto del contorno.

    [ST,ANGLE] = signature (B,x0,y0,f=1)

    """
    import numpy as np
    from matplotlib import pyplot as plt
    
    B  = np.int8(B)

    # 1 - Chequeo dimensiones de B
    nf = B.shape[0]
    nc = B.shape[1]
    if (nf < nc or nc != 2):
        print('B debe ser de tamaño nfx2')
    pass

    # 2 - Elimina el ultimo punto si es igual al primero
    if ( B[0,0] == B[nf-1,0] and B[0,1] == B[nf-1,1]):
        B = B[0:nf-1]
        nf = nf - 1
        print('Truncado por repetir punto de inicio y fin')
    pass

    # Arreglo abierto rho/theta
    rho = [] 
    theta = []
    for i in range(0,nf):
        
        # 3 - Cambio origen del sistema de coordenadas a (0, 0)
        B[i,0] = (B[i,0] - x0)  
        B[i,1] = (B[i,1] - y0)  
        
        # 4 - Cambio de coordenadas cartesianas a polares
        xc = B[i, 1]
        yc = -B[i, 0] #Cambio de signo del eje y

        # Magnitud
        rho.append(np.sqrt(xc**2 + yc**2))

        # Fase
        aux = np.rad2deg(np.arctan2(yc, xc))  # Convierte de radianes a grados
        # Convierte angulos negativos en positivos
        if (aux < 0):
            theta.append(aux + 360)
            pass
        else:
            # Convierte de radianes a grados
            theta.append(np.rad2deg(np.arctan2(yc, xc)))
            pass

        pass

    # Redondeo los angulos
    theta = np.round(theta)

    # 5 - Ordeno los angulos de menor a mayor y ordeno la magnitud respecto al 
    #   arreglo de angulos
    # Se encuentra el mayor valor de theta y su respectivo indice
    theta_aux = max(theta)
    i = np.where(theta == theta_aux) 
    i = i[0]
    # Se desplaza el arreglo theta hasta que queda ubicado el 0 en el origen
    roll_theta = np.roll(theta,nf-i-1) # max-i-1
    # Se desplaza el arreglo rho en la misma cantidad que theta para no perder 
    # la correlacion
    roll_rho = np.roll(rho,nf-i-1) # max-i-1

    # Se encuentra el mayor valor de rho para graficar
    rho_aux = max(rho)

    # Outputs
    ANGLE = theta
    ST = rho
    
    # Si el flag = 1 grafica
    if (f == 1):
       # Grafico
       plt.plot(roll_theta,roll_rho)
       plt.xlabel('Theta')
       plt.ylabel('Rho')
       plt.title('Grafico de la fira')
       plt.ylim(0,rho_aux)
       plt.show()
       pass
    
    return(ST,ANGLE)


import numpy as np
# Centro de prueba 
x0 = 6
y0 = 5

# Arreglo de prueba (cuadrado sentido horario) 
B = np.array([
              [4,3],
              [5,3],
              [6,3],
              [7,3],
              [8,3],
              [8,4],
              [8,5],
              [8,6],
              [8,7],
              [7,7],
              [6,7],
              [5,7],
              [4,7],
              [4,6],
              [4,5],
              [4,4]])

st,angle = signature (B,x0,y0,f=1)

