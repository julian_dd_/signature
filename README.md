# Signature

Función que calcula la firma de un objeto en python.

Los parametros de ingreso son:

    - Los puntos (i,j) del contorno del objeto en B.
    - Su centro en (x0,y0).
    - Flag f = 1 grafica la firma. Por defecto 1.

La funcion devuelve la magnitud (ST) y face (ANGLE) de cada punto del contorno.

[ST,ANGLE] = signature (B,x0,y0,f=1)


